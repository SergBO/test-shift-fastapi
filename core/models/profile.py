from datetime import datetime
from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column

from .base import Base


class Profile(Base):
    salary: Mapped[int] = mapped_column(default=0)
    salary_increase: Mapped[datetime]
    username_id: Mapped[str] = mapped_column(ForeignKey("users.username"))
