# Generate an RSA private key, of size 2048
openssl genrsa -out jwt-private.pem 2048

# Extract the public key from the key pair, whit can be used in cartificate
openssl rsa -in jwt-private.pem -outform PEM -pubout -out jwt-public.pem