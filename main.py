import uvicorn
from fastapi import FastAPI

from api_v1 import router as api_v1_router
from core.config import settings

app = FastAPI()

app.include_router(router=api_v1_router, prefix=settings.api_v1_prefix)


if __name__ == '__main__':
    uvicorn.run("main:app", reload=True)
