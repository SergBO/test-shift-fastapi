from datetime import datetime

from pydantic import BaseModel, EmailStr


class UserBase(BaseModel):
    username: str
    first_name: str
    last_name: str
    email: EmailStr


class UserCreate(UserBase):
    password: str


class UserOut(UserBase):
    id: int


class TokenInfo(BaseModel):
    access_token: str
    token_type: str = "Bearer"


class ProfileOut(BaseModel):
    username_id: str
    salary: int
    salary_increase: datetime

