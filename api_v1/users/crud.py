from datetime import datetime, timedelta

from sqlalchemy import select
from sqlalchemy.engine import Result
from sqlalchemy.ext.asyncio import AsyncSession
from auth.utils import hash_password
from core.models import User, Profile

from .schemas import UserCreate, UserOut


async def get_users(session: AsyncSession) -> list[User]:
    stmt = select(User).order_by(User.id)
    result: Result = await session.execute(stmt)
    users = result.scalars().all()
    return list(users)


async def get_get_user_by_id(session: AsyncSession, user_id: int) -> User | None:
    return await session.get(User, user_id)


async def get_user_by_username(session: AsyncSession, username: str) -> User | None:
    stmt = select(User).where(User.username == username)
    result: Result = await session.execute(stmt)
    user = result.scalar()
    return user


async def get_profile_by_username(session: AsyncSession, username: str) -> Profile:
    stmt = select(Profile).where(Profile.username_id == username)
    result: Result = await session.execute(stmt)
    profile = result.scalar()
    return profile


async def create_user(
        session: AsyncSession,
        user_in: UserCreate
) -> User:
    user_in.password = hash_password(user_in.password)
    user = User(**user_in.model_dump())
    session.add(user)

    await session.flush()
    await session.commit()
    await create_profile(session, user.username)

    return user


async def create_profile(
        session: AsyncSession,
        username: str
) -> Profile:
    profile = Profile(
        salary=10000,
        salary_increase=datetime.now() + timedelta(days=180),
        username_id=username
    )
    session.add(profile)
    await session.commit()

    return profile
