from sqlalchemy.ext.asyncio import AsyncSession

from fastapi import APIRouter, Depends

from api_v1.users.utils import validate_auth_user, create_access_token, get_username_by_token
from .schemas import UserCreate, UserOut, TokenInfo, ProfileOut

from core.models import db_helper
from . import crud


router = APIRouter(tags=["Employees"])


@router.post("/signup", response_model=UserOut)
async def create_user(
        user_in: UserCreate,
        session: AsyncSession = Depends(db_helper.session_dependency),
):
    user = await crud.create_user(session=session, user_in=user_in)
    return user


@router.post("/login/", response_model=TokenInfo)
def auth_user_issue_jwt(
        user: UserOut = Depends(validate_auth_user)
):
    access_token = create_access_token(user)
    return TokenInfo(
        access_token=access_token,
    )


@router.post("/salary", response_model=ProfileOut)
async def user_salary(
        data: dict = Depends(get_username_by_token),
        session: AsyncSession = Depends(db_helper.session_dependency),
):
    username = data["username"]
    profile = await crud.get_profile_by_username(session, username)

    return profile


@router.get("/all_users", response_model=list[UserOut])
async def all_users(
        session: AsyncSession = Depends(db_helper.session_dependency),
):
    return await crud.get_users(session)


@router.get("/user/{user_id}")
async def get_user(
        user_id: int,
        session: AsyncSession = Depends(db_helper.session_dependency),

) -> UserOut | None:
    return await crud.get_get_user_by_id(session, user_id)


@router.get("/check")
def health_check():
    return {
        "status": 200
    }
