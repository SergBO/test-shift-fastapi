from datetime import timedelta

from sqlalchemy.ext.asyncio import AsyncSession
from fastapi import (
    Depends,
    Form,
    HTTPException,
    status,
    Header
)
from core.config import settings
from core.models import db_helper
from . import crud
from auth import utils as auth_utils
from .schemas import UserOut

TOKEN_TYPE_FIELD = "type"
ACCESS_TOKEN_TYPE = "acceess"


async def validate_auth_user(
        username: str = Form(),
        password: str = Form(),
        session: AsyncSession = Depends(db_helper.session_dependency),
):
    unauthed_exc = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="invalad username or password"
    )
    user = await crud.get_user_by_username(session, username)

    if not user:
        raise unauthed_exc

    if not auth_utils.validate_password(
            password=password,
            hashed_password=user.password,
    ):
        raise unauthed_exc

    return user


def create_jwt(
        token_type: str,
        token_data: dict,
        expire_minutes: int = settings.auth_jwt.access_token_expire_minutes,
        expire_timedelta: timedelta | None = None,
) -> str:
    jwt_payload = {TOKEN_TYPE_FIELD: token_type}
    jwt_payload.update(token_data)
    return auth_utils.encode_jwt(
        payload=jwt_payload,
        expire_minutes=expire_minutes,
        expire_timedelta=expire_timedelta,
    )


def create_access_token(user: UserOut) -> str:
    jwt_payload = {
        "sub": user.id,
        "username": user.username
    }
    return create_jwt(
        token_type=ACCESS_TOKEN_TYPE,
        token_data=jwt_payload,
        expire_minutes=settings.auth_jwt.access_token_expire_minutes,
    )


def get_username_by_token(
        token: str | bytes = Header(alias="x-auth-token"),
):
    if not token:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Sign in for access"
        )
    decoded_token = auth_utils.decode_jwt(token)
    return decoded_token

